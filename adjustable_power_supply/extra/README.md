This is an extra PCB allowing to derive an extra 10V rail from the main DC 
supply feeding an independant volt/amp-meter, thus allowing to monitor the 
power supply voltage and intensity output.

It is a simple regulation circuit based on an LM317 adjustable linear regulator.
