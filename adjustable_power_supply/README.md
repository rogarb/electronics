This is a LM723-based 2 to 30V adjustable power supply with adjustable current 
limiting ( ~5mA to ~2A).

The design is based on schematics published by Frédéric Giamarchi available at
http://perso.iut-nimes.fr/fgiamarchi/wp-content/uploads/2016/09/Alim_723_V2.2.pdf.

See the following pages for a full description of the original project:
- http://perso.iut-nimes.fr/fgiamarchi/?p=352
- http://perso.iut-nimes.fr/fgiamarchi/?p=3683
