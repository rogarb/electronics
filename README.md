# electronics

This repo contains various personal electronics related projects, sometimes with 
related code for microcontroller or host program.

These designs come without any warranty at all. Check them and test them by 
yourself to make sure they fit your needs.

## Summary

- adjustable_power_supply: an LM723-based 2 to 30V adjustable power supply with 
  adjustable current limiting ( ~5mA to ~2A).
- usb_supply_5V_to_9V: a small 5V to 9V boost converter I designed to supply a
  small board with a microcontroller, needing around 25-30mA. 

