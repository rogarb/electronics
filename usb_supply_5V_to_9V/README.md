This is a 5V to 9V boost converter I designed using information provided by the
following websites:
 - https://en.wikipedia.org/wiki/Boost_converter
 - https://components101.com/articles/boost-converter-basics-working-design

I use it to replace a 9V battery supplying a small board with a microcontroller.
According to my measurements, the board needs around 25mA, with peaks to 30mA. 

